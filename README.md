# vuex-shopping-cart

> A shopping cart built with Vue 2 and Vuex 

## Tools

Caching - https://github.com/superwf/vuex-cache

Persist State - https://github.com/robinvdvleuten/vuex-persistedstate

UI Toolkit - https://github.com/ElemeFE/element


## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

## Created by Anatolii 2019-08-21